package com.example.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
class StringLnvl implements Serializable{
  private Character start;
  private Character end;

  public StringLnvl() {
  }

  public StringLnvl(Character start, Character end) {
    if (isValidCharacter(start) && isValidCharacter(end) && start <= end) {
      this.start = start;
      this.end = end;
    } else {
      throw new IllegalArgumentException("Invalid start or end characters for StringLnvl");
    }
  }

  public Character getStart() {
    return start;
  }

  public void setStart(Character start) {
    if (isValidCharacter(start) && start <= end) {
      this.start = start;
    } else {
      throw new IllegalArgumentException("Invalid start character for StringLnvl");
    }
  }

  public Character getEnd() {
    return end;
  }

  public void setEnd(Character end) {
    if (isValidCharacter(end) && start <= end) {
      this.end = end;
    } else {
      throw new IllegalArgumentException("Invalid end character for StringLnvl");
    }
  }

  public static boolean isValidCharacter(Character character) {
    // Перевірка, чи символ є літерою
    return Character.isLetter(character);
  }
}
class  Field implements Serializable {
  String name;
  Object value;

  public Field(String name, Object value) {
    this.name = name;
    if (validateValue(value)) {
      this.value = value;
    } else {
      System.out.println(value+" Not correct");
    }
  }

  private boolean validateValue(Object value) {
    if (value instanceof Integer ||
        value instanceof Double ||
        value instanceof String ||
        value instanceof Character ||
        value instanceof StringLnvl ||
        value instanceof File) {
      if (value instanceof File) {
        File file = (File) value;
        String extension = getFileExtension(file);
        return extension != null && (extension.equalsIgnoreCase("html") || extension.equalsIgnoreCase("htm"));
      }
      return true;
    }
    return false;
  }

  private String getFileExtension(File file) {
    String extension = null;
    String fileName = file.getName();
    int dotIndex = fileName.lastIndexOf('.');
    if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
      extension = fileName.substring(dotIndex + 1).toLowerCase();
    }
    return extension;
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Field field = (Field) obj;
    if (!name.equals(field.name)) {
      return false;
    }
    if (value == null) {
      return field.value == null;
    } else {
      return value.equals(field.value);
    }
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (value != null ? value.hashCode() : 0);
    return result;
  }
}



class Table implements Serializable {
  String name;
  Map<Integer, Map<String, Field>> rows;

  public Table(String name) {
    this.name = name;
    this.rows = new HashMap<>();
  }

  public void insertRow(int rowId, Map<String, Field> row) {
    rows.put(rowId, row);
  }

  public void deleteRow(int rowId) {
    rows.remove(rowId);
  }

  public Map<String, Field> getRow(int rowId) {
    return rows.get(rowId);
  }

  public void updateRow(int rowId, Map<String, Field> newRow) {
    rows.put(rowId, newRow);
  }
}

class Database implements Serializable {
  Map<String, Table> tables;

  public Database() {
    this.tables = new HashMap<>();
  }

  public void createTable(String tableName) {
    Table table = new Table(tableName);
    tables.put(tableName, table);
  }

  public void dropTable(String tableName) {
    tables.remove(tableName);
  }

  public void insertRow(String tableName, int rowId, Map<String, Field> row) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.insertRow(rowId, row);
    }
  }

  public void deleteRow(String tableName, int rowId) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.deleteRow(rowId);
    }
  }

  public Map<String, Field> getRow(String tableName, int rowId) {
    Table table = tables.get(tableName);
    if (table != null) {
      return table.getRow(rowId);
    }
    return null;
  }

  public void updateRow(String tableName, int rowId, Map<String, Field> newRow) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.updateRow(rowId, newRow);
    }
  }
    public Map<String, Field> getRowDifference(String tableName1, String tableName2, int rowId) {
      Map<String, Field> row1 = tables.get(tableName1).getRow(rowId);
      Map<String, Field> row2 = tables.get(tableName2).getRow(rowId);

      Map<String, Field> difference = new HashMap<>();

      for (Map.Entry<String, Field> entry : row1.entrySet()) {
        String fieldName = entry.getKey();
        Field field1 = entry.getValue();
        Field field2 = row2.get(fieldName);

        if (field2 == null || !field1.equals(field2)) {
          difference.put(fieldName, field1);
        }
      }

      for (Map.Entry<String, Field> entry : row2.entrySet()) {
        String fieldName = entry.getKey();
        if (!row1.containsKey(fieldName)) {
          difference.put(fieldName, entry.getValue());
        }
      }

      return difference;
    }
  public void saveToDisk(String fileName) {
    try (FileOutputStream fileOut = new FileOutputStream(fileName);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
      objectOut.writeObject(this);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Database loadFromDisk(String fileName) {
    try (FileInputStream fileIn = new FileInputStream(fileName);
        ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
      return (Database) objectIn.readObject();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
      return null;
    }
  }
}


public class Main {
  public static void main(String[] args) {
    Database db = new Database();

    // Створення таблиці "Students"
    db.createTable("Students");

    // Додавання даних в таблицю "Students"
    Map<String, Field> studentRow1 = new HashMap<>();
    studentRow1.put("ID", new Field("ID", 1));
    studentRow1.put("Name", new Field("Name", "John"));
    studentRow1.put("GPA", new Field("GPA", 3.8));

    db.insertRow("Students", 1, studentRow1);

    // Створення другої таблиці "Employees"
    db.createTable("Employees");

    // Додавання даних в таблицю "Employees"
    Map<String, Field> employeeRow1 = new HashMap<>();
    employeeRow1.put("ID", new Field("ID", 101));
    employeeRow1.put("Name", new Field("Name", "John"));
    employeeRow1.put("Salary", new Field("Salary", 50000));

    db.insertRow("Employees", 1, employeeRow1);

    // Отримання різниці між рядками двох таблиць
    Map<String, Field> difference = db.getRowDifference("Students", "Employees", 1);

    // Виведення різниці між рядками
    System.out.println("Difference between Students and Employees:");
    for (Map.Entry<String, Field> entry : difference.entrySet()) {
      System.out.println("Field: " + entry.getKey() + ", Value: " + entry.getValue().value);
    }

    // Збереження бази даних на диск
    db.saveToDisk("database.ser");

    // Завантаження бази даних з диску
    Database loadedDB = Database.loadFromDisk("database.ser");
  }
}
