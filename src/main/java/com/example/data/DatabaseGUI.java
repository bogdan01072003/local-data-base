package com.example.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javafx.scene.control.*;


public class DatabaseGUI extends Application {

  private Database db; // Передбачається, що у вас є екземпляр бази даних
  private Stage dataStage;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    db = Database.loadFromDisk("database.ser"); // Завантаження бази даних
    dataStage = new Stage();

    Button showDataButton = new Button("Show Data");
    showDataButton.setOnAction(event -> showData());

    VBox root = new VBox(10);
    root.getChildren().addAll(showDataButton);

    Scene scene = new Scene(root, 300, 200);
    primaryStage.setTitle("Database GUI");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  private void showData() {
    VBox root = new VBox(10);

    // Додамо кнопки для додавання та видалення таблиці
    Button addTableButton = new Button("Add Table");
    addTableButton.setOnAction(event -> addTable());

    Button deleteTableButton = new Button("Delete Table");
    deleteTableButton.setOnAction(event -> deleteTable());

    root.getChildren().addAll(addTableButton, deleteTableButton);

    for (Map.Entry<String, Table> entry : db.tables.entrySet()) {
      String tableName = entry.getKey();
      Label label = new Label(tableName);
      label.setOnMouseClicked(event -> showTableData(tableName));
      root.getChildren().add(label);
    }

    Scene dataScene = new Scene(root, 800, 800);
    dataStage.setScene(dataScene);
    dataStage.setTitle("Tables in Database");
    dataStage.show();
  }

  private void showTableData(String tableName) {
    Stage tableInfoStage = new Stage();
    tableInfoStage.setTitle("Table Info: " + tableName);

    VBox tableInfoRoot = new VBox(10);
    Table table = db.tables.get(tableName);

    for (Map.Entry<Integer, Map<String, Field>> rowEntry : table.rows.entrySet()) {
      Integer rowId = rowEntry.getKey();
      Map<String, Field> rowData = rowEntry.getValue();

      VBox rowBox = new VBox(5);

      Label rowLabel = new Label("Row ID: " + rowId);
      rowBox.getChildren().add(rowLabel);

      for (Map.Entry<String, Field> fieldEntry : rowData.entrySet()) {
        String fieldName = fieldEntry.getKey();
        Field fieldValue = fieldEntry.getValue();

        Label fieldLabel = new Label(fieldName + ": " + fieldValue.value);
        TextField fieldInput = new TextField(String.valueOf(fieldValue.value));

        fieldInput.setOnAction(e -> {
          // Збереження змін в базі даних при натисканні Enter
          fieldValue.value = fieldInput.getText();
          db.tables.get(tableName).rows.get(rowId).get(fieldName).value = fieldInput.getText();
        });

        rowBox.getChildren().addAll(fieldLabel, fieldInput);
      }

      tableInfoRoot.getChildren().add(rowBox);
    }

    // Кнопки для додавання та видалення рядків
    Button addRowButton = new Button("Add Row");
    addRowButton.setOnAction(e -> {
      int newRowId;
      if (table.rows.isEmpty()) {
        newRowId = 1; // Якщо таблиця порожня, починаємо з ID = 1
      } else {
        newRowId = Collections.max(table.rows.keySet()) + 1;
      }

      Map<String, Field> newRow = new HashMap<>();

      // Якщо є рядки, додамо їх поля в новий рядок
      if (!table.rows.isEmpty()) {
        Map<String, Field> sampleRow = table.rows.values().iterator().next();
        for (String fieldName : sampleRow.keySet()) {
          newRow.put(fieldName, new Field("",""));
        }
      }

      table.insertRow(newRowId, newRow);
      showTableData(tableName); // Оновлюємо вікно з даними таблиці
    });

    Button deleteRowButton = new Button("Delete Row");
    deleteRowButton.setOnAction(e -> {
      ChoiceDialog<Integer> dialog = new ChoiceDialog<>(table.rows.keySet().iterator().next(), table.rows.keySet());
      dialog.setTitle("Delete Row");
      dialog.setHeaderText("Choose row to delete:");
      dialog.setContentText("Row ID:");

      Optional<Integer> result = dialog.showAndWait();
      result.ifPresent(rowId -> {
        table.deleteRow(rowId);
        showTableData(tableName); // Оновлюємо вікно з даними таблиці
      });
    });

    // Кнопки для додавання та видалення полів
    Button addFieldButton = new Button("Add Field");
    addFieldButton.setOnAction(e -> {
      // Отримання ім'я нового поля через діалогове вікно (ваш код)
      TextInputDialog dialog = new TextInputDialog();
      dialog.setTitle("Add Field");
      dialog.setHeaderText("Enter new field name:");
      dialog.setContentText("Field Name:");

      Optional<String> result = dialog.showAndWait();
      result.ifPresent(fieldName -> {
        // Додавання нового поля в кожний існуючий рядок таблиці
        for (Map<String, Field> rowData : table.rows.values()) {
          rowData.put(fieldName, new Field("","")); // Ініціалізуємо нове поле порожнім значенням
        }
        showTableData(tableName); // Оновлюємо вікно з даними таблиці
      });
    });

    Button deleteFieldButton = new Button("Delete Field");
    deleteFieldButton.setOnAction(e -> {
      // Отримання ім'я поля для видалення через діалогове вікно (ваш код)
      ChoiceDialog<String> dialog = new ChoiceDialog<>(table.rows.entrySet().iterator().next().getValue().keySet().iterator().next(), table.rows.entrySet().iterator().next().getValue().keySet());
      dialog.setTitle("Delete Field");
      dialog.setHeaderText("Choose field to delete:");
      dialog.setContentText("Field Name:");

      Optional<String> result = dialog.showAndWait();
      result.ifPresent(fieldName -> {
        // Видалення поля з кожного рядка таблиці
        for (Map<String, Field> rowData : table.rows.values()) {
          rowData.remove(fieldName);
        }
        showTableData(tableName); // Оновлюємо вікно з даними таблиці
      });
    });

    Button saveChangesButton = new Button("Save Changes");
    saveChangesButton.setOnAction(e -> {
      db.saveToDisk("database.ser");
      System.out.println("Changes saved to database.");
    });

    Button compareTablesButton = new Button("Compare Tables");
    compareTablesButton.setOnAction(e -> {
      // Отримуємо дві таблиці для порівняння
      ChoiceDialog<String> dialog = new ChoiceDialog<>(db.tables.keySet().iterator().next(), db.tables.keySet());
      dialog.setTitle("Compare Tables");
      dialog.setHeaderText("Choose tables to compare:");
      dialog.setContentText("Table Name:");

      Optional<String> result = dialog.showAndWait();
      result.ifPresent(secondTableName -> {
        TextInputDialog rowIdDialog = new TextInputDialog();
        rowIdDialog.setTitle("Row ID");
        rowIdDialog.setHeaderText("Enter Row ID to compare:");
        rowIdDialog.setContentText("Row ID:");

        Optional<String> rowIdResult = rowIdDialog.showAndWait();
        rowIdResult.ifPresent(rowIdString -> {
          try {
            int rowId = Integer.parseInt(rowIdString);
            Map<String, Field> difference = db.getRowDifference(tableName, secondTableName, rowId);
            if (!difference.isEmpty()) {
              // Якщо є різниця між рядками, виводимо результат
              Stage differenceStage = new Stage();
              differenceStage.setTitle("Row Difference");
              VBox differenceRoot = new VBox(10);

              for (Map.Entry<String, Field> entry : difference.entrySet()) {
                Label differenceLabel = new Label(entry.getKey() + ": " + entry.getValue().value);
                differenceRoot.getChildren().add(differenceLabel);
              }

              Scene differenceScene = new Scene(differenceRoot, 300, 200);
              differenceStage.setScene(differenceScene);
              differenceStage.show();
            } else {
              // Якщо немає різниці, виводимо повідомлення
              Alert alert = new Alert(Alert.AlertType.INFORMATION);
              alert.setTitle("Comparison Result");
              alert.setHeaderText(null);
              alert.setContentText("No differences found between the rows.");
              alert.showAndWait();
            }
          } catch (NumberFormatException ex) {
            // Помилка у введенні Row ID
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Invalid Row ID. Please enter a valid number.");
            alert.showAndWait();
          }
        });
      });
    });

    tableInfoRoot.getChildren().addAll(addRowButton, deleteRowButton, addFieldButton, deleteFieldButton, saveChangesButton, compareTablesButton);

    Scene tableInfoScene = new Scene(tableInfoRoot, 700, 700);
    tableInfoStage.setScene(tableInfoScene);
    tableInfoStage.show();
  }


  private void addTable() {
    TextInputDialog dialog = new TextInputDialog();
    dialog.setTitle("Add Table");
    dialog.setHeaderText("Enter table name:");
    dialog.setContentText("Table Name:");

    Optional<String> result = dialog.showAndWait();
    result.ifPresent(tableName -> {
      // Додавання нової таблиці до бази даних
      db.createTable(tableName);
      System.out.println("Table '" + tableName + "' added to the database.");
      // Оновлення вікна з переліком таблиць
      showData();
    });
  }

  private void deleteTable() {
    ChoiceDialog<String> dialog = new ChoiceDialog<>(db.tables.keySet().iterator().next(),
        db.tables.keySet());
    dialog.setTitle("Delete Table");
    dialog.setHeaderText("Choose table to delete:");
    dialog.setContentText("Table:");

    Optional<String> result = dialog.showAndWait();
    result.ifPresent(tableName -> {
      // Видалення обраної таблиці з бази даних
      db.dropTable(tableName);
      System.out.println("Table '" + tableName + "' deleted from the database.");
      // Оновлення вікна з переліком таблиць
      showData();
    });
  }
}